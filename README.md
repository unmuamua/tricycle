# `tricycle` experimental library

## agda equipments

naming conventions in *enterprise* style:
- (most of) type identifiers are in `[Brackets]`
- (most of) module identifiers are in `⟨Angles⟩`
- constructors have emphasis with `Bang!`
- levels are started with `:Colon`
- level types are started with `#Hash`
- level functions are distinguished with `Colon:`
- and also file names have `=Wings=`
