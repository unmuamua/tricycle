{-# OPTIONS --no-import-sorts #-}

import Agda.Primitive as ⟨A-Prim⟩
import Agda.Builtin.Unit as ⟨A-Unit⟩
import Agda.Builtin.Nat as ⟨A-Nat⟩
import Agda.Builtin.FromNat as ⟨A-FromNat⟩
import Agda.Builtin.Equality as ⟨A-Equality⟩

module =Base= where

open ⟨A-Prim⟩ public
  using (_⊔_)
  renaming (Set to [Type] ; Prop to [Prop] ; Setω to [Type]∞)
  renaming (Level to #Type ; lsuc to Type: ; lzero to ∅)

open ⟨A-Unit⟩ public
  using ()
  renaming (⊤ to [Unit] ; tt to Unit!)

open ⟨A-Nat⟩ public
  using ()
  renaming (Nat to [Nat] ; zero to Zero! ; suc to Succ!)

open ⟨A-FromNat⟩ public
  using (fromNat)
  renaming (Number to [FromNat])

open ⟨A-Equality⟩ public
  using (_≡_)
  renaming (refl to ⊜)

private
 variable
  :A :B :C : #Type

[Level] : [Type]₁
[Level] = [Type]₀

record [∫] (A : [Type] :A) {#B : [Type]} (B: : #B → #Type) : [Type]∞ where
  field _≫_ : A → ∀ :B → [Type] (B: :B)
open [∫] {{…}} public

instance
  ∫:Type : [∫] ([Type] :A) (λ :B → :A ⊔ Type: :B)
  ∫:Type = record {
    _≫_ = λ A :B → (A → [Type] :B) }

module ⟨Unit⟩ where
  [Unit!] : [Type] :A ≫ :A
  [Unit!] A = A
  module _ {A : [Type] :A} (UnitA : [Unit!] A) where
    rec : [Unit] → A
    rec Unit! = UnitA

module ⟨Nat⟩ where
  [Zero!] : [Type] :A ≫ :A
  [Zero!] A = A
  [Succ!] : [Type] :A ≫ :A
  [Succ!] A = A → A
  module _ {A : [Type] :A} (ZeroA : [Zero!] A) (SuccA : [Succ!] A) where
    rec : [Nat] → A
    rec Zero! = ZeroA
    rec (Succ! N) = SuccA (rec N)

instance
  FromNat:Nat : [FromNat] [Nat]
  FromNat:Nat = record {
    Constraint = λ _ → [Unit] ;
    fromNat = λ n → n }

instance
  FromNat:#Type : [FromNat] #Type
  FromNat:#Type = record {
    Constraint = λ _ → [Unit] ;
    fromNat = λ n → ⟨Nat⟩.rec ∅ Type: n }
