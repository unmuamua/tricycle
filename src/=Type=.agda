open import =Base=

module =Type= where

private
 variable
  :A :B :C :R₁ :R₂ :R₁₂ :R₂₃ :O₁ :O₂ :O₃ : #Type

module Prelude where
  record _[~]_ (A : [Type] :A) (B : [Type] :B) : [Type] (:A ⊔ :B) where
    constructor _~_
    field ₁ : A
    field ₂ : B
  open _[~]_ public

  [2~]_ : [Type] :A → [Type] :A
  [2~]_ A = A [~] A

  [Dup~] : [Type] :A [~] [Type] :B → [Type] (:A ⊔ :B)
  [Dup~] (A ~ B) = A [~] B

  [=] : {A : [Type] :A} → [2~] A → [Type] :A
  [=] (a₁ ~ a₂) = a₁ ≡ a₂

  module _
    {O₁ : [Type] :O₁} {O₂ : [Type] :O₂} {O₃ : [Type] :O₃}
    (R₁₂ : O₁ [~] O₂ → [Type] :R₁₂) (R₂₃ : O₂ [~] O₃ → [Type] :R₂₃)
    ((«₁» ~ «₃») : O₁ [~] O₃)
   where
    record _[∘]_  : [Type] (:O₂ ⊔ :R₁₂ ⊔ :R₂₃) where
      constructor _∘_
      field {«₂»} : O₂
      field ₁~₂ : R₁₂ («₁» ~ «₂»)
      field ₂~₃ : R₂₃ («₂» ~ «₃»)
    open _[∘]_ public

  module _ {A : [Type] :A} {B : [Type] :B}
    (R₁ : (A [~] B) → [Type] :R₁) (R₂ : (A [~] B) → [Type] :R₂)
   where

    _→′_ : [Type] (:A ⊔ :B ⊔ :R₁ ⊔ :R₂)
    _→′_ = ∀ {ab} → R₁ ab → R₂ ab

    record _[~]′_ : [Type] (:A ⊔ :B ⊔ :R₁ ⊔ :R₂) where
      constructor _~′_
      field {ab} : A [~] B
      field ₁ : R₁ ab
      field ₂ : R₂ ab

open Prelude public

module ⟨Type⟩ where
  module _ {:Obj'} where
    [Obj] = [Type] :Obj'
  module _ {:Obj' :Mor} (2Obj : [2~] [Obj] {:Obj'}) where
    [Mor] = [Dup~] 2Obj → [Type] :Mor
  module _ {:Obj' :Mor} {O : [Obj] {:Obj'}}
    ((R₁ ~ R₂) : [2~] [Mor] {:Obj'} {:Mor} (O ~ O))
   where
    [id] = [=] →′ R₂
    [mu] = (R₁ [∘] R₁) →′ R₂

module Type (:Obj) where
  open ⟨Type⟩ public

  Obj : [Obj]
  Obj = [Type] :Obj
  Fun : [Mor] (Obj ~ Obj)
  Fun (A ~ B) = A → B
  Rel : [Mor] (Obj ~ Obj)
  Rel 2O = [Dup~] 2O → [Type] :Obj

  id:Fun : [id] (Fun ~ Fun)
  id:Fun ⊜ = λ o → o
  mu:Fun : [mu] (Fun ~ Fun)
  mu:Fun (f₁ ∘ f₂) = λ o → (f₂ (f₁ (o)))

  id:Rel : [id] (Rel ~ Rel)
  id:Rel ⊜ = [=]
  mu:Rel : [mu] (Rel ~ Rel)
  mu:Rel (R₁ ∘ R₂) = R₁ [∘] R₂
