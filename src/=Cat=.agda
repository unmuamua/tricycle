open import =Base=
open import =Type=

module =Cat= where

record #Cat : [Level] where
  constructor #!
  field :El : #Type
  -- field :To : #Type

module _ (# : #Cat) where
  open #Cat #
  module ⟨Cat⟩  where
    module U = Type :El
    [El] = U.Obj
    module _ (El : [El]) where
      [To] = U.Rel (El ~ El)
    module _ {El : [El]} (To : [To] El) where
      [id] = U.[id] (To ~ To)
      [mu] = U.[mu] (To ~ To)
  record [Cat] : [Type] (Type: :El) where
    constructor Cat!
    open ⟨Cat⟩
    field El : [El]
    field To : [To] El
    field id : [id] To

module Cat (# : #Cat) where
  open #Cat #
  module U = Type :El
  -- module U' = Type (Type: (:El ⊔ :To)) (_)

  Obj : [Type] _
  Obj = [Cat] #

  module _ ((A ~ B) : [2~] Obj) where
    record [Fun] : [Type] :El where
      constructor CatFun!
      open [Cat]
      field el: : U.Fun (A .El ~ B .El)
      -- field to: : U.Fun|Rel (A .To ~ B .To) (el: ~ el:)
